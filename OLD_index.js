var email = require("./lib/email");
var mongoClient = require("mongodb").MongoClient;
//var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var log = 1;
//var notification = require('./lib/notification.js');
//var patch = require('./lib/patch.js');
//var test = require('./lib/test.js');
var from = 'tales.fund <info@talents.fund>';
var timeoutQueue;
var url = "mongodb://project:project@localhost:27017/project";

mongoClient.connect(url, function(err, db) {
    if (err) return console.log(err);

    clearTimeout(timeoutQueue);
    timeoutQueue = setTimeout(hung, 86400*1000);
    // хрень
    //patch.users_created_at(db);

    // Registration 2
    var time = parseInt(new Date().getTime() / 1000);
    db.collection("users").find({
        "first_name": {$ne: null},
        "email_trusted": {$ne: 1},
        "created_at": {$ne: 0},
        "type_profile": {$in: ["Student"]}
    }, {"_id": 1, "email": 1,"type_profile": 1}).forEach(function (object) {
        db.collection("email_notifications").find({"uid": object._id, "type_profile": object.type_profile, "email_type": "Registration"}).count(function (err, count) {
            if(count === 0){
                //console.log("net zapisi");
                db.collection("email_notifications").insertMany([{"uid": object._id, "type_profile": object.type_profile, "email_type": "Registration", "status": false, "created_at": time}]);
                if (object.email){
                    //console.log("/lkasfgdk'lrg");
                    var mailOptions = {
                        "from": from,
                        "to": object.email
                    };
                    email.registrationS(mailOptions, object);
                }
            }else{
                //console.log("est zapis");
            }
        })
        //console.log(object._id);
    })
    // Registration 3
    db.collection("users").find({
        "first_name": {$ne: null},
        "email_trusted": {$ne: 1},
        "created_at": {$ne: 0},
        "type_profile": {$in: ["Company"]}
    }, {"_id": 1, "email": 1,"type_profile": 1}).forEach(function (object) {
        db.collection("email_notifications").find({"uid": object._id, "type_profile": object.type_profile, "email_type": "Registration"}).count(function (err, count) {
            if(count === 0){
                //console.log("net zapisi");
                db.collection("email_notifications").insertMany([{"uid": object._id, "type_profile": object.type_profile, "email_type": "Registration", "status": false, "created_at": time}]);
                if (object.email){
                    //console.log("/lkasfgdk'lrg");
                    var mailOptions = {
                        "from": from,
                        "to": object.email
                    };
                    email.registrationC(mailOptions, object);
                }
            }else{
                //console.log("est zapis");
            }
        })
        //console.log(object._id);
    })
    // Registration 4
    db.collection("users").find({
        "first_name": {$ne: null},
        "email_trusted": {$ne: 1},
        "created_at": {$ne: 0},
        "type_profile": {$in: ["Educator"]}
    }, {"_id": 1, "email": 1,"type_profile": 1}).forEach(function (object) {
        db.collection("email_notifications").find({"uid": object._id, "type_profile": object.type_profile, "email_type": "Registration"}).count(function (err, count) {
            if(count === 0){
                //console.log("net zapisi");
                db.collection("email_notifications").insertMany([{"uid": object._id, "type_profile": object.type_profile, "email_type": "Registration", "status": false, "created_at": time}]);
                if (object.email){
                    //console.log("/lkasfgdk'lrg");
                    var mailOptions = {
                        "from": from,
                        "to": object.email
                    };
                    email.registrationE(mailOptions, object);
                }
            }else{
                //console.log("est zapis");
            }
        })
        //console.log(object._id);
    })
    // Bookmarked Vacancy Application 5
    var time = parseInt(new Date().getTime() / 1000);
    db.collection("vacancy").find({
        "deadLine": {
            $lte: time + 3600 * 24 * 5,
            $gte: time
        }
        }, {"_id": 1,"deadLine":1}).toArray(function (err, results) {
        results.forEach(function (value) {
            db.collection("bookmarks").find({"items.resumeId": ObjectId(value._id)}, {"uid": 1}).toArray(function (err, items) {
                items.forEach(function (item) {

                    //notification.Bookmarked Vacancy Application(db, [{"_id": item.uid, "type_profile": "Student"}]);
                    db.collection("users").find({
                        "email_trusted": {$ne: 1},
                        "created_at": {$ne: 0},
                        //"type_profile": {$in: ["Student"]},
                        "_id": ObjectId(item.uid)
                    }, {"_id": 1, "email": 1,"type_profile": 1}).forEach(function (object) {
                        db.collection("email_notifications").find({"uid": object._id, "type_profile": object.type_profile, "email_type": "bookmarkedVacancyApplicationS"}).count(function (err, count) {
                            if(count === 0){
                                //console.log("net zapisi");
                                //db.collection("email_notifications").insertMany([{"uid": object._id, "type_profile": object.type_profile, "email_type": "bookmarkedVacancyApplicationS", "status": false, "created_at": time}]);
                                if (object.email){
                                    //console.log("/lkasfgdk'lrg");
                                    var mailOptions = {
                                        "from": from,
                                        "to": object.email
                                    };
                                    email.bookmarkedVacancyApplicationS(mailOptions, object);
                                }
                            }else{
                                //console.log("est zapis");
                            }
                        })
                        //console.log(object._id);
                    })
                })

            });

        });
    });
    // Unread Message S 6
    var time = parseInt(new Date().getTime() / 1000);
    db.collection("messages").find({
        "date": {$lte: time - 3600 * 24 * 3},
        "status": 0
    }, {"receiver": 1}).toArray(function (err, results) {
        results.forEach(function (value) {
            db.collection("users").find({"_id": value.receiver, "type_profile": "Student"}).count(function (err, count) {
                if (count > 0) {
                    //.newMessage(db, [{"_id": value.receiver, "type_profile": "Student"}]);
                    db.collection("users").find({
                        "created_at": {$ne: 0},
                        "email_trusted": {$ne: 1},
                        //"type_profile": {$in: ["Student"]},
                        "_id": ObjectId(value.receiver)
                    }, {"_id": 1, "email": 1,"type_profile": 1}).forEach(function (object) {
                        db.collection("email_notifications").find({"uid": object._id, "type_profile": object.type_profile, "email_type": "newMessageS"}).count(function (err, count) {
                            if(count === 0){
                                //console.log("net zapisi");
                                //db.collection("email_notifications").insertMany([{"uid": object._id, "type_profile": object.type_profile, "email_type": "bookmarkedVacancyApplicationS", "status": false, "created_at": time}]);
                                if (object.email){
                                    //console.log("/lkasfgdk'lrg");
                                    var mailOptions = {
                                        "from": from,
                                        "to": object.email
                                    };
                                    email.newMessageS(mailOptions, object);
                                }
                            }else{
                                //console.log("est zapis");
                            }
                        })
                        //console.log(object._id);
                    })
                }
            });
        });
    });

    //Unread Message C 7
    // не делать

    // Happy Birthday 8
    // нет даты рождения

/*
    db.getCollection('notification').aggregate([
        {$match: {
                "options_notification": {$in: [10,11,12,13,14,15,16,20,22,23,26,28,29,30,31,33,34]}
                //"created_at": {$gte: 1517223063 - 3600 * 24 * 30}
            }},
        { $group : {
                "_id" : "$users_id",
                "count": { $sum: 1 }
            }},
        {$match: {
                "count": {$gte: 5}
            }},
    ])
*/
    /*var time = parseInt(new Date().getTime() / 1000);
    db.collection("notification").aggregate([
            {$match: {
                    "options_notification": {$in: [10,11,12,13,14,15,16,20,22,23,26,28,29,30,31,33,34]}
                    //"created_at": {$gte: 1517223063 - 3600 * 24 * 30}
            }},
            { $group : {
                "_id" : "$users_id",
                "count": { $sum: 1 }
            }},
            {$match: {
                "count": {$gte: 5}
            }},
    ]).forEach(function (object) {

        console.log(object);
        db.collection("users").find({"_id": object._id},{"email": 1, "_id": 0}).forEach(function (object2) {
            console.log(object2);
        })
    });*/




    // end connect
});



function hung() {
    console.log('time out!! ');
    clearTimeout(timeoutQueue);
    timeoutQueue=setTimeout(hung, 86400*1000);
}

function log(message) {
    console.log(message);
}