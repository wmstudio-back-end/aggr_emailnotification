var async    = require('async');
var ObjectId = require('mongodb').ObjectID;

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

exports.sendEmail = function (data, tpl, callback) {
    var arrF = []
    var f    = function (ob) {
        var t = function (callback) {
            transport(ob, tpl, callback)
        }
        arrF.push(t)
    }
    for (var i = 0; i < data.length; i++) {
        f(data[i])
    }
    async.series(arrF, callback)
}

function transport(object, tpl, callback) {
    if (!validateEmail(object.email)) {
        callback(null, "invalid email " + object.email);
        return
    }
    // callback(null, "SEND TO " + object.email)
    global.api.post({
        data: object,
        tpl:  tpl
    }, 'sendEmailNotification', function (error, res) {
        global.log.info('SEND TPL',tpl,error,res);
        if (error) {
            global.log.info(error);
        }
        else {
            global.log.info('updateEmail');
            global.db.collection("email_notifications").update({"uid": ObjectId(object.uid)}, {$pullAll: {"emails": {'types': tpl}}}, function (err, opt) {
                global.log.info('updateEmail1');
                global.db.collection("email_notifications").update({"uid": ObjectId(object.uid)}, {
                    $addToSet: {
                        "emails": {
                            'types':      tpl,
                            'updated_at': parseInt(new Date().getTime() / 1000),
                            'options':    object.opt
                        }
                    }
                }, {upsert: true}, function (err, opt) {
                    global.log.info('updateEmail2');
                    global.log.error(err);
                    callback(null, "SEND TO " + object.email)
                })
            })
        }
    })
}
