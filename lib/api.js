/**
 * Created by term on 30.03.18.
 */

var request = require('request');

var protocol = global.argv.hasOwnProperty('https')?'https://':'http://'

module.exports = {
    post:     function (data, method, next) {
        if (typeof global.config.serv_cms_host === "undefined")
            global.config.serv_cms_host = 'talents.fund';
        var headers = {
                'secret':       global.env.secret,
                'content-type': 'application/json; charset=utf-8',
            },
            url     = global.config.serv_cms_host + ((typeof global.config.serv_cms_port !== "undefined") ? ':' + global.config.serv_cms_port : ''),
            options = {
                url:     protocol + url + '/api',
                auth:    {
                    'user': 'talents',
                    'pass': 'dev_wms',
                },
                headers: headers,
                method:  "POST",
                json:    true,
                rejectUnauthorized: false,
                requestCert: true,
                agent: false,
                body:    {
                    data:   data,
                    method: method
                }
            };

        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('body', body)
                if (typeof next === 'function')
                    next(null, body);
            } else {
                if (typeof next === 'function')
                    next(null,'error')
                console.log(error, body)
                console.log('url',protocol + url + '/api')
            }
        });
    }
}

function state() {

}