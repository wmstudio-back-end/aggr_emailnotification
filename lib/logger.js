var fs           = require('fs');
var path         = require('path');
var winston      = require('winston');
var log_filename = 'emailnotification_log.log';
var config       = {
    levels: {
        error:   0,
        debug:   1,
        warn:    2,
        data:    3,
        info:    4,
        verbose: 5,
        silly:   6,
        custom:  7
    },
    colors: {
        error:   'red',
        debug:   'blue',
        warn:    'yellow',
        data:    'grey',
        info:    'green',
        verbose: 'cyan',
        silly:   'magenta',
        custom:  'yellow'
    }
};

winston.addColors(config.colors);

module.exports = winston.createLogger({
    levels:     config.levels,
    format:     winston.format.combine(winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }), winston.format.json(), winston.format.prettyPrint(), winston.format(function dynamicContent(info, opts) {
            if (typeof info.message !== 'string') info.message = JSON.stringify(info.message)
            if (typeof info[Symbol.for('splat')] !== "undefined") {
                for (var i = 0; info[Symbol.for('splat')].length > i; i++) {
                    info.message += '\n %O'
                }
            }
            return info;
        })(), winston.format.splat(), //
        // The simple format outputs
        // `${level}: ${message} ${[Object with everything else]}`
        //
        winston.format.colorize(), // winston.format.simple(),
        //
        // Alternatively you could use this custom printf format if you
        // want to control where the timestamp comes in your final message.
        // Try replacing `format.simple()` above with this:
        //
        winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)),
    transports: [
        new winston.transports.Console({handleExceptions: true}),
        new winston.transports.File({filename: log_filename})
    ],
    level:      'info',
});