/**
 * "1 есть регистрация
 2 была публикация вакансии/практики
 3 последняя публикация вакансии/практики более 3х месяцев назад
 4 Е Майл ""No Vacancy Post"" не высылался или последний раз был отправлен более 3х месяцев назад"
 * @param callback
 */

module.exports = function (callback) {
    global.db.collection('users').aggregate([
        {
            $match: {
                "type_profile": "Company",
                "Company.auth": true
            }
        },
        {
            $lookup: {
                from:         "vacancy",
                localField:   "uid",
                foreignField: "_id",
                as:           "vacancy"
            },
        },
        {
            $match: {
                "vacancy": {
                    $exists:    true,
                    $elemMatch: {
                        published_at: {
                            $lte: global.time - 86400 * 30 * 2
                        }
                    }
                },
            }
        },
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "_id",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            $match: {
                $or: [
                    {
                        "email_notifications": {
                            $size: 0
                        }
                    },
                    {
                        "email_notifications": {
                            $exists: true,
                            $all:    [
                                {
                                    $elemMatch: {
                                        $or: [
                                            {
                                                "emails.options.17uid": "$_id",
                                                "emails.updated_at":    {
                                                    $lte: global.time - 86400 * 30 * 3 - 3600
                                                }
                                            },
                                            {
                                                "emails.options.17uid": {$ne: "$_id"},
                                            },
                                            {
                                                'emails.options.17uid': {
                                                    $exists: false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            $project: {
                "_id":             0,
                "first_name":      "$first_name",
                "last_name":       "$last_name",
                "nameCompany":     "$Company.nameCompany",
                "email":           "$email",
                "type_profile":    "$type_profile",
                "uid":             "$_id",
                "currentLang":     "$currentLang",
                "prefLang":        "$prefLang",
                "eduPrefLang":     "$Educator.prefLang",
                "companyPrefLang": "$Company.prefLang",
                "studentPrefLang": "$Student.prefLang",
                "opt":             {
                    "17uid":        "$_id",
                    "email":        "$email",
                    "type_profile": "$type_profile"
                }, //"email_notifications.emails": "$email_notifications.emails",
            }
        },
    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<17>");

        global.log.info(data);
        if (err) {
            global.log.info("method m_17 error", err);
            callback(null, err);
        }
        else {
            global.mail.sendEmail(data, 17, callback);
        }

    });
}