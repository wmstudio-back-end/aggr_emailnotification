var async     = require('async');

/**
 *
 * "1 сегодня заканчивается один из сервисов ниже:
 Premium listing
 Highlighted frame "
 *
 * @param callback
 */

module.exports = function (cb) {
    async.series([
        function (callback) {
            global.db.collection("vacancy").aggregate([
                {
                    $match: {
                        "PROMpremiumListing": {
                            $gte: global.time,
                            $lte: global.time + 86400
                        }
                    }
                },
                {
                    $lookup: {
                        "from":         "users",
                        "localField":   "uid",
                        "foreignField": "_id",
                        "as":           "users"
                    }
                },
                {"$unwind": "$users"},
                {
                    $project: {
                        "_id":             "$_id",
                        "vid":             "$_id",
                        "positionTitle":   "$positionTitle",
                        "uid":             "$users._id",
                        "first_name":      "$users.first_name",
                        "last_name":       "$users.last_name",
                        "currentLang":     "$users.currentLang",
                        "prefLang":        "$users.prefLang",
                        "eduPrefLang":     "$users.Educator.prefLang",
                        "companyPrefLang": "$users.Company.prefLang",
                        "studentPrefLang": "$users.Student.prefLang",
                        "email":           "$users.email",
                        "type_profile":    "$users.type_profile"
                    }
                },
                {
                    $lookup: {
                        "from":         "email_notifications",
                        "localField":   "uid",
                        "foreignField": "uid",
                        "as":           "email_notifications"
                    }
                },
                {
                    $match: {
                        $or: [
                            {
                                "email_notifications": {
                                    $exists: false
                                }
                            },
                            {
                                "email_notifications": {
                                    $exists: true,
                                    $all:    [
                                        {
                                            $elemMatch: {
                                                $or: [
                                                    {
                                                        "emails.options.13vid": "$_id",
                                                        "emails.updated_at":    {
                                                            $lte: time - 82800
                                                        }
                                                    },
                                                    {
                                                        "emails.options.13vid": {$ne: "$_id"},
                                                    },
                                                    {
                                                        'emails.options.13vid': {
                                                            $exists: false
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                },
                            },
                        ]
                    }
                },
                {
                    $project: {
                        "_id":                        "$_id",
                        "first_name":                 "$first_name",
                        "last_name":                  "$last_name",
                        "vid":                        "$vid",
                        "positionTitle":              "$positionTitle",
                        "email":                      "$email",
                        "type_profile":               "$type_profile",
                        "uid":                        "$uid",
                        "currentLang":                "$currentLang",
                        "prefLang":                   "$prefLang",
                        "eduPrefLang":                "$eduPrefLang",
                        "companyPrefLang":            "$companyPrefLang",
                        "studentPrefLang":            "$studentPrefLang",
                        "opt":                        {
                            "13uid":        "$uid",
                            "13vid":        "$_id",
                            "email":        "$email",
                            "type_profile": "$type_profile"
                        },
                        "email_notifications.emails": "$email_notifications.emails",
                    }
                },
            ], function (err, data) {
                // email
                // currentLang
                global.log.info("<13>");
                if (data && data.length) {
                    data = data.map(function (e, i) {
                        e.service_name = 'promotions.prem_listing';
                        return e;
                    });
                }
                global.log.info(data);
                if (err) {
                    global.log.info("method m_13 error", err);
                    callback(null, err);
                }
                else {
                    mail.sendEmail(data, 13, callback);
                }

            });
        },
        function (callback) {
            global.db.collection("vacancy").aggregate([
                {
                    $match: {
                        "PROMhightFrame": {
                            $gte:  global.time,
                            $lte: global.time + 86400
                        }
                    }
                },
                {
                    $lookup: {
                        "from":         "users",
                        "localField":   "uid",
                        "foreignField": "_id",
                        "as":           "users"
                    }
                },
                {"$unwind": "$users"},
                {
                    $project: {
                        "_id":             "$_id",
                        "vid":             "$_id",
                        "positionTitle":   "$positionTitle",
                        "uid":             "$users._id",
                        "first_name":      "$users.first_name",
                        "last_name":       "$users.last_name",
                        "currentLang":     "$users.currentLang",
                        "prefLang":        "$users.prefLang",
                        "eduPrefLang":     "$users.Educator.prefLang",
                        "companyPrefLang": "$users.Company.prefLang",
                        "studentPrefLang": "$users.Student.prefLang",
                        "email":           "$users.email",
                        "type_profile":    "$users.type_profile"
                    }
                },
                {
                    $lookup: {
                        "from":         "email_notifications",
                        "localField":   "uid",
                        "foreignField": "uid",
                        "as":           "email_notifications"
                    }
                },
                {
                    $match: {
                        $or: [
                            {
                                "email_notifications": {
                                    $exists: false
                                }
                            },
                            {
                                "email_notifications": {
                                    $exists: true,
                                    $all:    [
                                        {
                                            $elemMatch: {
                                                $or: [
                                                    {
                                                        "emails.options.13vid": "$_id",
                                                        "emails.updated_at":    {
                                                            $lte: time - 82800
                                                        }
                                                    },
                                                    {
                                                        "emails.options.13vid": {$ne: "$_id"},
                                                    },
                                                    {
                                                        'emails.options.13vid': {
                                                            $exists: false
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                },
                            },
                        ]
                    }
                },
                {
                    $project: {
                        "_id":                        "$_id",
                        "first_name":                 "$first_name",
                        "last_name":                  "$last_name",
                        "vid":                        "$vid",
                        "positionTitle":              "$positionTitle",
                        "email":                      "$email",
                        "type_profile":               "$type_profile",
                        "uid":                        "$uid",
                        "currentLang":                "$currentLang",
                        "prefLang":                   "$prefLang",
                        "eduPrefLang":                "$eduPrefLang",
                        "companyPrefLang":            "$companyPrefLang",
                        "studentPrefLang":            "$studentPrefLang",
                        "opt":                        {
                            "13uid":        "$uid",
                            "13vid":        "$_id",
                            "email":        "$email",
                            "type_profile": "$type_profile"
                        },
                        "email_notifications.emails": "$email_notifications.emails",
                    }
                },
            ], function (err, data) {
                // email
                // currentLang
                global.log.info("<13>");

                if (data && data.length) {

                    data = data.map(function (e, i) {
                        e.service_name = 'promotions.frame';
                        return e;
                    });
                }
                global.log.info(data);

                if (err) {
                    global.log.info("method m_13 error", err);
                    callback(null, err);
                }
                else {
                    global.mail.sendEmail(data, 13, callback);
                }
            });
        },
    ], cb);
}
