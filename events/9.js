/**
 *
 * "1 Имеется более 5 нотификаций 1го приоритета
 2 Емайл ""Critical notification"" не высылался или последний раз был отправлен более 1 месяца назад"
 *
 * include m_10
 * @param callback
 */
module.exports = function (callback) {
    global.db.collection("notification").aggregate([
        {
            $match: {
                "options_notification": {
                    $in: [
                        10,
                        11,
                        12,
                        13,
                        14,
                        15,
                        16,
                        20,
                        22,
                        23,
                        3,
                        33,
                        31,
                        30,
                        29,
                        28,
                        26
                    ]
                }
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "users_id",
                "foreignField": "_id",
                "as":           "users"
            }
        },
        {"$unwind": "$users"},
        {
            $match: {
                "users.type_profile": "Company"
            }
        },
        {
            $project: {
                "_id":             0,
                "uid":             "$users._id",
                "first_name":      "$users.first_name",
                "last_name":       "$users.last_name",
                "currentLang":     "$users.currentLang",
                "prefLang":        "$users.prefLang",
                "email":           "$users.email",
                "type_profile":    "$users.type_profile",
                "eduPrefLang":     "$users.Educator.prefLang",
                "companyPrefLang": "$users.Company.prefLang",
                "studentPrefLang": "$users.Student.prefLang",
            }
        },
        {
            $group: {
                "_id":   {
                    "uid":          "$uid",
                    "first_name":   "$first_name",
                    "last_name":    "$last_name",
                    "currentLang":  "$currentLang",
                    "type_profile": "$type_profile",
                    "email":        "$email"
                },
                "count": {$sum: 1}
            }
        },
        {$match: {count: {$gte: 5}}},
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "_id.uid",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            $match: {
                $or: [
                    {
                        "email_notifications": {
                            $size: 0
                        }
                    },
                    {
                        "email_notifications": {
                            $exists: true,
                            $all:    [
                                {
                                    $elemMatch: {
                                        $or: [
                                            {
                                                "emails.options.9uid": "$_id.uid",
                                                "emails.updated_at":   {
                                                    $lte: time - 86400 * 30 - 3600
                                                }
                                            },
                                            {
                                                "emails.options.9uid": {$ne: "$_id.uid"},
                                            },
                                            {
                                                'emails.options.9uid': {
                                                    $exists: false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            $project: {
                "_id":             0,
                "first_name":      "$_id.first_name",
                "last_name":       "$_id.last_name",
                "email":           "$_id.email",
                "type_profile":    "$_id.type_profile",
                "uid":             "$_id.uid",
                "currentLang":     "$_id.currentLang",
                "prefLang":        "$_id.prefLang",
                "eduPrefLang":     "$_id.eduPrefLang",
                "companyPrefLang": "$_id.companyPrefLang",
                "studentPrefLang": "$_id.studentPrefLang",
                "opt":             {
                    "9uid":         "$_id.uid",
                    "email":        "$_id.email",
                    "type_profile": "$_id.type_profile"
                }
            }
        }

    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<9>");

        if (data && data.length) {
            var issetUsers = [];
            data           = data.filter(function (e, i, a) {
                if (issetUsers.indexOf(e.uid) > -1) return false;
                issetUsers.push(e.uid);
                return true;
            });
        }

        global.log.info(data);
        if (err) {
            global.log.info("method m_9 error", err);
            callback(null, err);
        }
        else {
            global.mail.sendEmail(data, 9, callback);
        }
    });
}