/**
 *
 * 1 сегодня заканчивается опубликованная вакансия
 *
 * @param callback
 */

module.exports = function (callback) {
    global.db.collection("vacancy").aggregate([
        {
            $match: {
                "deadLine": {
                    $gte: global.time,
                    $lte: global.time + 86400
                }
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "uid",
                "foreignField": "_id",
                "as":           "users"
            }
        },
        {"$unwind": "$users"},
        {
            $project: {
                "_id":             "$_id",
                "vid":             "$_id",
                "positionTitle":   "$positionTitle",
                "uid":             "$users._id",
                "first_name":      "$users.first_name",
                "last_name":       "$users.last_name",
                "currentLang":     "$users.currentLang",
                "prefLang":        "$users.prefLang",
                "eduPrefLang":     "$users.Educator.prefLang",
                "companyPrefLang": "$users.Company.prefLang",
                "studentPrefLang": "$users.Student.prefLang",
                "email":           "$users.email",
                "type_profile":    "$users.type_profile"
            }
        },
        {
            $lookup: {
                "from":         "subscribeVacancy",
                "localField":   "_id",
                "foreignField": "vacancyId",
                "as":           "subscribeVacancy"
            }
        },
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "uid",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            "$unwind": {
                path:                         "$email_notifications",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $project: {
                "_id":                 "$_id",
                "first_name":          "$first_name",
                "last_name":           "$last_name",
                "email":               "$email",
                "type_profile":        "$type_profile",
                "uid":                 "$uid",
                "positionTitle":       "$positionTitle",
                "subscribe_count":     {$size: "$subscribeVacancy"},
                "currentLang":         "$currentLang",
                "prefLang":            "$prefLang",
                "eduPrefLang":         "$eduPrefLang",
                "companyPrefLang":     "$companyPrefLang",
                "studentPrefLang":     "$studentPrefLang",
                "opt":                 {
                    "14uid":        "$uid",
                    "14vid":        "$_id",
                    "email":        "$email",
                    "type_profile": "$type_profile"
                },
                "email_notifications": "$email_notifications"
            }
        }

    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<14>");

        global.log.info(data);
        if (err) {
            global.log.info("method m_14 error", err);
            callback(null, err);
        }
        else {
            var items = data.filter((e) => {
                if (!e.email_notifications) {
                    return true;
                }

                try {
                    var filter = true;
                    e.email_notifications.emails.forEach((email) => {
                        if (email.options.hasOwnProperty('14uid') && e._id.toString() == email.options['14uid'].toString() && parseInt(email.updated_at) > global.time - 82400) {
                            filter = false;
                            return false;
                        }
                    });
                    return filter;
                } catch (err) {
                    console.log(err);
                    return false;
                }
            });
            console.log(items);
            global.mail.sendEmail(data, 14, callback);
        }

    });
}
