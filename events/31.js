/**
 *
 * "1 Новая вакансия сохранена 3 дня назад
 2 Вакасния никогда не была отправлена на модерацию"
 *
 * @param callback
 */

module.exports = function (callback) {
    global.db.collection('vacancy').aggregate([
        {
            $match: {
                "proof":      0,
                "updated_at": {$lte: global.time - 86400 * 3}
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "uid",
                "foreignField": "_id",
                "as":           "users"
            }
        },
        {"$unwind": "$users"},
        {
            $project: {
                "first_name":      "$users.first_name",
                "last_name":       "$users.last_name",
                "positionTitle":   "$vacancy.positionTitle",
                "email":           "$users.email",
                "type_temp":       "$type_temp",
                "uid":             "$users._id",
                "type_profile":    "$type_profile",
                "currentLang":     "$users.currentLang",
                "eduPrefLang":     "$users.Educator.prefLang",
                "companyPrefLang": "$users.Company.prefLang",
                "studentPrefLang": "$users.Student.prefLang",
            }
        },
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "uid",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            $match: {
                $or: [
                    {
                        "email_notifications": {
                            $size: 0
                        }
                    },
                    {
                        "email_notifications": {
                            $exists: true,
                            $all:    [
                                {
                                    $elemMatch: {
                                        $or: [
                                            {
                                                "emails.options.31vid": "$_id",
                                                "emails.updated_at":    {
                                                    $lte: time - 82400
                                                }
                                            },
                                            {
                                                "emails.options.31vid": {$ne: "$_id"},
                                            },
                                            {
                                                'emails.options.31vid': {
                                                    $exists: false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            $project: {
                "_id":                        "$_id",
                "first_name":                 "$first_name",
                "last_name":                  "$last_name",
                "email":                      "$email",
                "type_profile":               "$type_profile",
                "type_temp":                  "$type_temp",
                "uid":                        "$uid",
                "positionTitle":              "$positionTitle",
                "currentLang":                "$currentLang",
                "prefLang":                   "$prefLang",
                "eduPrefLang":                "$eduPrefLang",
                "companyPrefLang":            "$companyPrefLang",
                "studentPrefLang":            "$studentPrefLang",
                "opt":                        {
                    "31uid": "$uid",
                    "email": "$email",
                    "31vid": "$_id"
                },
                "email_notifications.emails": "$email_notifications.emails",
            }
        },
        {
            $match: {
                isSend: false
            }
        }
    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<31>");

        global.log.info(data);
        if (err) {
            global.log.info("method m_31 error", err);
            callback(null, err);
        }
        else {
            global.mail.sendEmail(data, 31, callback);
        }

    });
}
