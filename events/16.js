/**
 *
 * "1 есть регистрация
 2 нет заказа/счета более 2 месяцев или последний счет был оплачен более 3 месяцев назад
 3 Емаил ""Retention Company"" не высылался или последний раз был отправлен более 2х месяцев назад"
 *
 * @param callback
 */

module.exports = function (callback) {
    global.db.collection('users').aggregate([
        {
            $match: {
                "type_profile": "Company",
                "Company.auth": true,
            }
        },
        {
            $lookup: {
                from:         "user_order",
                localField:   "_id",
                foreignField: "uid",
                as:           "user_orders"
            },
        },
        {
            $match: {
                user_orders: {
                    $exists: true
                }, // $or:         [
                //     {
                //         user_orders: {
                //             $all: [
                //                 {
                //                     $elemMatch: {
                //                         created_at: {
                //                             $lte: time - 86400 * 30 * 3
                //                         },
                //                         status:     1
                //                     }
                //                 }
                //             ]
                //         },
                //     },
                //     {
                //         user_orders: {
                //             $all: [
                //                 {
                //                     $elemMatch: {
                //                         created_at: {
                //                             $lte: time - 86400 * 30 * 2
                //                         },
                //                         status:     0
                //                     }
                //                 }
                //             ]
                //         },
                //     }
                // ]
            }
        },
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "_id",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            "$unwind": {
                path:                         "$email_notifications",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $project: {
                "_id":                 "$_id",
                "first_name":          "$first_name",
                "last_name":           "$last_name",
                "nameCompany":         "$Company.nameCompany",
                "email":               "$email",
                "type_profile":        "$type_profile",
                "uid":                 "$_id",
                "currentLang":         "$currentLang",
                "prefLang":            "$prefLang",
                "eduPrefLang":         "$Educator.prefLang",
                "companyPrefLang":     "$Company.prefLang",
                "studentPrefLang":     "$Student.prefLang",
                "user_orders":         "$user_orders",
                "created_at":          "$created_at",
                "opt":                 {
                    "16uid":        "$_id",
                    "email":        "$email",
                    "type_profile": "$type_profile"
                },
                "email_notifications": "$email_notifications"
            }
        }
    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<16>");

        // global.log.info(data);
        if (err) {
            global.log.info("method m_16 error", err);
            callback(null, err);
        }
        else {
            var items = data.filter((e) => {
                let filter = true;
                try {
                    if (!e.user_orders.length && parseInt(e.created_at) > global.time - 84600 * 30 * 2){
                        filter = false;
                        return false;
                    } else {
                        e.user_orders.forEach((order) => {
                            if ((order.status == 1 && parseInt(order.created_at) > global.time - 84600 * 30 * 3) || (order.status == 0 && parseInt(order.created_at) > global.time - 84600 * 30 * 2)) {
                                filter = false;
                                return false;
                            }
                        });
                    }
                } catch (err) {
                    global.log.info(err);
                    filter = false;
                    return false;
                }

                if (!filter){
                    return false;
                }

                if (!e.email_notifications) {
                    return true;
                }

                try {
                    filter = true;
                    e.email_notifications.emails.forEach((email) => {
                        if (email.options.hasOwnProperty('16uid') && e._id.toString() == email.options['16uid'].toString() && parseInt(email.updated_at) > global.time - 86400 * 30 * 2 - 3600) {
                            filter = false;
                            return false;
                        }
                    });
                    return filter;
                } catch (err) {
                    console.log(err);
                    return false;
                }
            });
            global.log.info(items);
            global.mail.sendEmail(items, 16, callback);
            // callback(null, null);
        }

    });
};
