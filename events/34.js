/**
 *
 * "1 в CV соискателя отмечено что он ещё учится (не оконьчил) в уч.заведении где подтверждена его оценка
 2 от даты Grade dated прошло 6 месяцев
 3 подтвержденный статус оценки изменился на неподтвержденный
 4 новая оценка не добавлена"
 *
 * TODO Добавить логику для 3,4
 * @param callback
 */

module.exports = function (callback) {

    var gradeDateTime = new Date(new Date() - new Date(1000 * 86400 * 180));
    var gradeEndTime1 = ('0' + gradeDateTime.getDate()).slice(-2) + '/' + gradeDateTime.getFullYear();
    gradeDateTime     = new Date(new Date() - new Date(1000 * 86400 * 181));
    var gradeEndTime2 = ('0' + gradeDateTime.getDate()).slice(-2) + '/' + gradeDateTime.getFullYear();

    global.db.collection('school_user_data').aggregate([
        {
            $match: {
                "SchoolProgressEducation.status":       {
                    $in: [
                        1,
                        2,
                        '1',
                        '2'
                    ]
                },
                "SchoolProgressEducation.gradeStarted": {
                    $in: [
                        gradeEndTime1,
                        gradeEndTime2
                    ]
                },
                "endDateMonth":                         {$exists: false},
                "endDateYear":                          {$exists: false}
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "uid",
                "foreignField": "_id",
                "as":           "users"
            }
        },
        {"$unwind": "$users"},
        {
            $project: {
                "_id":             "$_id",
                "first_name":      "$users.first_name",
                "last_name":       "$users.last_name",
                "email":           "$users.email",
                "uid":             "$users._id",
                "schoolName":      "$schoolName",
                "currentLang":     "$users.currentLang",
                "prefLang":        "$users.prefLang",
                "eduPrefLang":     "$users.Educator.prefLang",
                "companyPrefLang": "$users.Company.prefLang",
                "studentPrefLang": "$users.Student.prefLang",
            }
        },
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "uid",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            $match: {
                $or: [
                    {
                        "email_notifications": {
                            $size: 0
                        }
                    },
                    {
                        "email_notifications": {
                            $exists: true,
                            $all:    [
                                {
                                    $elemMatch: {
                                        $or: [
                                            {
                                                "emails.options.34sid": "$_id",
                                                "emails.updated_at":    {
                                                    $lte: time - 86400 * 179 - 3600
                                                }
                                            },
                                            {
                                                "emails.options.34sid": {$ne: "$_id"},
                                            },
                                            {
                                                'emails.options.34sid': {
                                                    $exists: false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            $project: {
                "_id":                        0,
                "first_name":                 "$first_name",
                "last_name":                  "$last_name",
                "email":                      "$email",
                "uid":                        "$uid",
                "currentLang":                "$currentLang",
                "prefLang":                   "$prefLang",
                "eduPrefLang":                "$eduPrefLang",
                "companyPrefLang":            "$companyPrefLang",
                "studentPrefLang":            "$studentPrefLang",
                "schoolName":                 "$schoolName",
                "sid":                        "$_id",
                "opt":                        {
                    "34uid": "$uid",
                    "34sid": "$_id",
                    "email": "$email"
                },
                "email_notifications.emails": "$email_notifications.emails",
            }
        },
    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<34>");

        global.log.info(data);
        if (err) {
            global.log.info("method m_34 error", err);
            callback(null, err);
        }
        else {
            global.mail.sendEmail(data, 34, callback);
        }

    });
}