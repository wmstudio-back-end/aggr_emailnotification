/**
 *
 * "1 поступило новое сообщение
 2 сообщение не прочитано
 3 на это сообщение не высылался email ранее"
 *
 * @param callback
 */
module.exports = function (callback) {
    global.db.collection("messages").aggregate([
        {
            $match: {
                "date":   {$gte: global.time - 3600},
                "status": 0
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "receiver",
                "foreignField": "_id",
                "as":           "receiver_user"
            }
        },
        {"$unwind": "$receiver_user"},
        {
            $match: {
                "receiver_user.type_profile": "Educator"
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "sender",
                "foreignField": "_id",
                "as":           "sender_user"
            }
        },
        {"$unwind": "$sender_user"},
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "receiver_user._id",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            "$unwind": {
                path:                         "$email_notifications",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $project: {
                "_id":                 "$_id",
                "photo":               "$receiver_user.Photo",
                "first_name":          "$sender_user.first_name",
                "last_name":           "$sender_user.last_name",
                "msg":                 "$text",
                "email":               "$receiver_user.email",
                "uid":                 "$receiver_user._id",
                "currentLang":         "$receiver_user.currentLang",
                "prefLang":            "$receiver_user.prefLang",
                "eduPrefLang":         "$receiver_user.Educator.prefLang",
                "companyPrefLang":     "$receiver_user.Company.prefLang",
                "studentPrefLang":     "$receiver_user.Student.prefLang",
                "opt":                 {
                    "8uid":       "$receiver_user._id",
                    "email":      "$receiver_user.email",
                    "message_id": "$_id",
                    "text":       "$text"
                },
                "email_notifications": "$email_notifications"
            }
        },
        {
            $match: {
                $or: [
                    {
                        email_notifications: null
                    },
                    {
                        email_notifications:                             {
                            $ne: null
                        },
                        'email_notifications.emails.options.message_id': {
                            $nin: [
                                "$_id",
                            ]
                        }
                    }
                ]
            }
        }
    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<8>");

        global.log.info(data);

        if (err) {
            global.log.info("method m_8 error", err);
            callback(null, err);
        }
        else {
            global.mail.sendEmail(data, 8, callback);
        }
    });
}
