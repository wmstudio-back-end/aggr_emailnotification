
/**
 *
 * "1 Есть сохраненный поиск с активированной рассылкой на почту
 2 Появились новые CV удовлетворяющие поиску. Новые- опубликованные за период от последнего отправления емайла ""Search Candidates"" по данному поиску до настоящего времени.
 3 Емайл ""Search Candidates"" по данному поиску не высылался или последний раз был отправлен более периода указанного в настройке сохранения поиска назад"
 *
 * "1 Есть сохраненный поиск с активированной рассылкой на почту
 2 Появились новые вакансии удовлетворяющие поиску. Новые- опубликованные за период от последнего отправления емайла ""Search Vacancies"" по данному поиску до настоящего времени.
 3 Емайл ""Search Vacancies"" по данному поиску не высылался или последний раз был отправлен более периода указанного в настройке сохранения поиска назад"
 *
 * include m_19
 * @param callback
 */
module.exports = function (callback) {
    global.db.collection('packages').find({
        'settings.notify': true
    }).toArray(function (err, packages) {
        if (err) {
            global.log.error(err);
            callback(null, err);
        }
        else {
            if (!(packages && packages.length)) return getSaveSearches(null, callback);

            getSaveSearches(packages.map((e) => {
                return ObjectId(e._id);
            }), callback);

        }
    });
}

function getSaveSearches(packagesId, callback) {
    var current = new Date();
    var period  = [0];

    if (current.getDay() == 0) {
        period.push(1);
    }

    if (current.getDate() == 1) {
        period.push(2);
    }

    var aggregate = [
        {
            $match: {
                "alertEmail": true,
                "period":     {$in: period},
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "uid",
                "foreignField": "_id",
                "as":           "users"
            }
        },
    ];

    if (packagesId) {
        aggregate.push({
            $match: {
                $or: [
                    {
                        'users.type_profile':             'Company',
                        "users.service.active.packageId": {
                            $in: packagesId
                        }
                    },
                    {
                        'users.type_profile': 'Student',
                    }
                ]
            }
        });
    }
    else {
        aggregate.push({
            $match: {
                'users.type_profile': 'Student',
            }
        });
    }

    aggregate.push({"$unwind": "$users"});
    aggregate.push({
        $project: {
            "_id":             "$_id",
            "ssid":            "$_id",
            "uid":             "$users._id",
            "first_name":      "$users.first_name",
            "last_name":       "$users.last_name",
            "name":            "$name",
            "currentLang":     "$users.currentLang",
            "prefLang":        "$users.prefLang",
            "eduPrefLang":     "$users.Educator.prefLang",
            "companyPrefLang": "$users.Company.prefLang",
            "studentPrefLang": "$users.Student.prefLang",
            "count_total":     "$count_total",
            "count_unread":    "$count_unread",
            "count_new":       "$count_new",
            "query":           "$query",
            "email":           "$users.email",
            "type_profile":    "$users.type_profile"
        }
    });
    aggregate.push({
        $lookup: {
            "from":         "email_notifications",
            "localField":   "uid",
            "foreignField": "uid",
            "as":           "email_notifications"
        }
    });
    // aggregate.push({
    //     $match: {
    //         $or: [
    //             {
    //                 "email_notifications": {
    //                     $size: 0
    //                 }
    //             },
    //             {
    //                 "email_notifications": {
    //                     $exists: true,
    //                     $all:    [
    //                         {
    //                             $elemMatch: {
    //                                 $or: [
    //                                     {
    //                                         "emails.options.18ssid": "$$ROOT._id",
    //                                         "emails.updated_at":     {
    //                                             $lte: time - 82400
    //                                         }
    //                                     },
    //                                     {
    //                                         "emails.options.18ssid": {$ne: "$$ROOT._id"},
    //                                     },
    //                                     {
    //                                         'emails.options.18ssid': {
    //                                             $exists: false
    //                                         }
    //                                     }
    //                                 ]
    //                             }
    //                         }
    //                     ]
    //                 }
    //             }
    //         ]
    //     }
    // });
    aggregate.push({
        $project: {
            "_id":             "$_id",
            "first_name":      "$first_name",
            "last_name":       "$last_name",
            "name":            "$name",
            "email":           "$email",
            "type_profile":    "$type_profile",
            "uid":             "$uid",
            "currentLang":     "$currentLang",
            "prefLang":        "$prefLang",
            "eduPrefLang":     "$eduPrefLang",
            "companyPrefLang": "$companyPrefLang",
            "studentPrefLang": "$studentPrefLang",
            "ids_total":       "$count_total",
            "ids_unread":      "$count_unread",
            "ids_new":         "$count_new",
            "query":           "$query",
            "opt":             {
                "18ssid":       "$_id",
                "email":        "$email",
                "type_profile": "$type_profile"
            }
        }
    });


    global.db.collection('savedSearches').aggregate(aggregate, function (err, data) {
        // email
        // currentLang
        global.log.info("<18>");
        if (!err) {
            var arrF = [];

            data.forEach(function (e, i) {

                if (!Array.isArray(e.ids_total)) e.ids_total = [];

                if (!Array.isArray(e.ids_unread)) e.ids_unread = [];

                var ids   = _.uniq(JSON.parse(JSON.stringify(e.ids_total.concat(e.ids_unread))));
                var total = ids;
                if (ids.length) {
                    ids = ids.map(function (el, i) {
                        return ObjectId(el);
                    });
                }

                if (e.type_profile == 'Company') {
                    arrF.push(function (cb) {
                        try {
                            newResumes(JSON.parse(e.query), ids, function (data) {
                                if (data && data.length) {
                                    e.resumes = data;
                                    async.series([
                                        function (callb) {
                                            global.mail.sendEmail([e], 18, callb);
                                        },
                                        function (callb) {
                                            updateSaveSearch(total, e);
                                            callb(null, true);
                                        }
                                    ], cb);
                                }
                                else {
                                    cb(null, 'no found resumes');
                                }
                            });
                        } catch (er) {
                            global.log.error(er);
                            cb(null, 'no found resumes');
                        }
                    });
                }
                else {
                    arrF.push(function (cb) {
                        try {
                            newVacancies(JSON.parse(e.query), ids, function (data) {
                                if (data && data.length) {
                                    e.vacancies = data;
                                    async.series([
                                        function (callb) {
                                            global.mail.sendEmail([e], 19, callb);
                                        },
                                        function (callb) {
                                            updateSaveSearch(total, e);
                                            callb(null, true);
                                        }
                                    ], cb);
                                }
                                else {
                                    cb(null, 'no found vacancies');
                                }
                            });
                        } catch (er) {
                            global.log.error(er);
                            cb(null, 'no found vacancies');
                        }
                    });
                }
            });

            async.series(arrF, callback);

            global.log.info(data);
        }
        else {
            callback(null, true);
            global.log.info("method m_18  error", err);
        }

    });
}

function newResumes(query, ids, next) {
    var queryUser           = null,
        queryRecommendation = null,
        queryResume         = {},
        querySchool         = null,
        queryLocation       = null;

    if (query.hasOwnProperty('resume')) {
        queryResume = query['resume'];
    }
    if (query.hasOwnProperty('user')) {
        queryResume = getOtherQuery(queryResume, query['user'], 'users');
    }
    if (query.hasOwnProperty('recommendation')) {
        queryResume = getOtherQuery(queryResume, query['recommendation'], 'recommendation');
    }
    if (query.hasOwnProperty('school')) {
        queryResume = getOtherQuery(queryResume, query['school'], 'schools');
    }
    if (query.hasOwnProperty('location')) {
        queryResume = getOtherQuery(queryResume, query['location'], 'location');
    }

    queryResume['_id'] = {"$nin": ids};

    queryResume.publish = {
        '$exists': true,
        '$in':     [
            1,
            true,
            'true'
        ]
    };
    // vars.queryResume['recommends.status'] = 1
    // vars.queryResume['recommends.visibility'] = true
    var items = {
        "_id":        1,
        "name":       1,
        "uid":        1,
        "created_at": 1,
        "updated_at": 1,
        "status":     1,
        "publish":    1,
        "prc":        1,
        "rating":     1,
        "users":      1,
    };

    var search       = null;
    var searchQuery  = {};
    var advanceQuery = {};

    if (queryResume.hasOwnProperty('$text')) {
        search = JSON.parse(JSON.stringify(queryResume['$text']));
        delete queryResume['$text'];
        searchQuery = search;
    }

    if (queryResume.hasOwnProperty('jobOptions.locations.addr')) {
        queryResume['jobOptions.locations.addr']['$in'] = queryResume['jobOptions.locations.addr']['$in'].map(function (e, i, a) {
            return new RegExp(e, 'i');
        });
    }

    if (queryResume.hasOwnProperty('internshipOptions.locations.addr')) {
        queryResume['internshipOptions.locations.addr']['$in'] = queryResume['internshipOptions.locations.addr']['$in'].map(function (e, i, a) {
            return new RegExp(e, 'i');
        });
    }

    if (queryResume.hasOwnProperty('users.Location.addr')) {
        queryResume['users.Location.addr']['$in'] = queryResume['users.Location.addr']['$in'].map(function (e, i, a) {
            return new RegExp(e, 'i');
        });
    }

    if (queryResume.hasOwnProperty('jobOptions.locations.addr') && queryResume.hasOwnProperty('internshipOptions.locations.addr')) {
        advanceQuery = {
            $or: [
                {
                    'jobOptions.locations.addr': queryResume['jobOptions.locations.addr']
                },
                {
                    'internshipOptions.locations.addr': queryResume['internshipOptions.locations.addr']
                }
            ]
        };
        delete queryResume['jobOptions.locations.addr'];
        delete queryResume['internshipOptions.locations.addr'];
    }

    var aggregate = [

        {
            $lookup: {
                "from":         "school_user_data",
                "localField":   "uid",
                "foreignField": "uid",
                "as":           "schools"
            }
        },
        {
            $lookup: {
                "from":         "recommendation",
                "localField":   "uid",
                "foreignField": "uuid",
                "as":           "recommends"
            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "uid",
                "foreignField": "_id",
                "as":           "users"
            }
        },
        {
            $lookup: {
                "from":         "locations",
                "localField":   "uid",
                "foreignField": "uid",
                "as":           "location"
            }
        },
        {$match: queryResume},
        {$match: searchQuery},
        {$match: advanceQuery},
        {$project: items}
    ];

    aggregate.push({
        $group: {
            _id:      {"uid": "$uid"},
            "resume": {"$first": "$$ROOT"}
        }
    });
    console.log(JSON.stringify(aggregate));

    global.db.collection('resume').aggregate(aggregate, function (err, us) {
        global.log.info('aggregate', JSON.stringify(aggregate));
        if (err) {
            global.log.info('[ALERT] prototype.getResume pcDB.resume');
            global.log.error(err);
            next([]);
        }
        else {
            try {
                if (us && us.length) {
                    us = us.map(function (e, i, a) {
                        return e.resume;
                    });
                    next(us);
                }
                else {
                    next([]);
                }
            } catch (err) {
                global.log.error(err);
                next([]);
            }
        }
    });
}

function getOtherQuery(query, otherQuery, name) {
    var keys = Object.keys(otherQuery);
    if (!keys.length) return query;
    for (var i of keys) {
        if (i == '$or') {
            query[i] = otherQuery[i];
            continue;
        }
        query[name + '.' + i] = otherQuery[i];
    }
    if (name == 'location') query['location.type'] = 'profile';
    return query;
}



function updateSaveSearch(total, e) {
    if (e) {
        if (typeof e.resumes !== "undefined" && e.resumes.length) {
            var newIds = e.resumes.map((e) => {
                return ObjectId(e._id);
            });
        }
        else if (typeof e.vacancies !== "undefined" && e.vacancies.length) {
            var newIds = e.vacancies.map((e) => {
                return ObjectId(e._id);
            });
        }
        var count_new  = !_.isArray(e.ids_new) ? newIds : _.uniq(newIds.concat(e.ids_new)).map((e) => {
            return typeof e === "string" ? ObjectId(e) : e;
        });
        var total_new  = !_.isArray(e.ids_total) ? newIds : _.uniq(newIds.concat(e.ids_total)).map((e) => {
            return typeof e === "string" ? ObjectId(e) : e;
        });
        var unread_new = !_.isArray(e.ids_unread) ? newIds : _.uniq(newIds.concat(e.ids_unread)).map((e) => {
            return typeof e === "string" ? ObjectId(e) : e;
        });

        global.db.collection('savedSearches').updateOne({_id: ObjectId(e._id)}, {
            $set: {
                "count_total":  total_new,
                "count_unread": unread_new,
                "ids":          total_new,
                "count_new":    count_new,
            }
        }, (err, data) => {
            global.log.info('update save search', err, e._id, newIds, total_new);
        });
    }
}

function newVacancies(q, ids, next) {
    var query    = q;
    query['_id'] = {
        $nin: ids
    };

    if (q.hasOwnProperty('projectID')) {
        query.projectID = ObjectId(q.projectID);
    }

    if (query.hasOwnProperty('$text')) {
        var searchQuery = JSON.parse(JSON.stringify(query['$text']));
        delete query['$text'];
        if (query.hasOwnProperty('$or')) {
            query['$or'].push(searchQuery);
        }
        else {
            query['$or'] = searchQuery['$or'];
        }
    }

    if (query.hasOwnProperty('address')) {
        query.address['$in'] = query.address['$in'].map(function (e, i, a) {
            return new RegExp(e, 'i');
        });
    }

    query.publish  = true;
    query.proof    = 2;
    query.deadLine = {$gte: Math.round(+new Date() / 1000)};

    global.log.info(JSON.stringify(query));

    global.db.collection('vacancy').aggregate([
        {
            $lookup: {
                "from":         "users",
                "localField":   "uid",
                "foreignField": "_id",
                "as":           "user"
            }
        },
        {
            $match: query
        }
    ], function (err, data) {
        if (err || !data || (data && !data.length)) {
            global.log.error('newVacancies', err);
            next([]);
        }
        else {
            data = data.map(function (e) {
                e.users = e.user;
                return e;
            });
            next(data);
        }
    });
}