/**
 *
 * "1 ни одно CV кандидата не обновлялась более 6 месяцев
 2 данное письмо не высылалось последние 6 месяцев "
 *
 * @param callback
 */

module.exports = function (callback) {
    global.db.collection('users').aggregate([
        {
            $match: {
                "type_profile": "Student",
            }
        },
        {
            $lookup: {
                "from":         "resume",
                "localField":   "_id",
                "foreignField": "uid",
                "as":           "resumes"
            }
        },
        {
            $match: {
                "resumes": {
                    $exists: true
                },
                $all:      {
                    $elemMatch: {
                        "updated_at": {$lte: global.time - 86400 * 180}
                    }
                }
            }
        },
        {
            $project: {
                "first_name":      "$first_name",
                "last_name":       "$last_name",
                "email":           "$email",
                "uid":             "$_id",
                "currentLang":     "$currentLang",
                "prefLang":        "$prefLang",
                "eduPrefLang":     "$Educator.prefLang",
                "companyPrefLang": "$Company.prefLang",
                "studentPrefLang": "$Student.prefLang",
            }
        },
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "_id",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            $match: {
                $or: [
                    {
                        "email_notifications": {
                            $size: 0
                        }
                    },
                    {
                        "email_notifications": {
                            $exists: true,
                            $all:    [
                                {
                                    $elemMatch: {
                                        $or: [
                                            {
                                                "emails.options.33uid": "$uid",
                                                "emails.updated_at":    {
                                                    $lte: time - 86400 * 179 - 3600
                                                }
                                            },
                                            {
                                                "emails.options.33uid": {$ne: "$uid"},
                                            },
                                            {
                                                'emails.options.33uid': {
                                                    $exists: false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            $project: {
                "_id":             0,
                "first_name":      "$first_name",
                "last_name":       "$last_name",
                "email":           "$email",
                "uid":             "$uid",
                "currentLang":     "$currentLang",
                "prefLang":        "$prefLang",
                "eduPrefLang":     "$eduPrefLang",
                "companyPrefLang": "$companyPrefLang",
                "studentPrefLang": "$studentPrefLang",
                "opt":             {
                    "33uid": "$uid",
                    "email": "$email"
                },
            }
        }
    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<33>");

        global.log.info(data);
        if (err) {
            global.log.info("method m_33 error", err);
            callback(null, err);
        }
        else {
            global.mail.sendEmail(data, 33, callback);
        }
    });
}