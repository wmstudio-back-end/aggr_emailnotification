/**
 *
 * "- 1 Вакансия занесена в избранные
 - 2 Срок окончания приема заявок заканчивается менее чем через 5 дней
 - 3 Е Майл ""Bookmarked Vacancy Application"" не высылался по данной вакансии"
 *
 * @param callback
 */

module.exports = function (callback) {
    global.db.collection("bookmarks").aggregate([
        {
            $match: {
                "items.type": 0
            }
        },
        {
            $unwind: "$items"
        },
        {
            $lookup: {
                "from":         "vacancy",
                "localField":   "items.resumeId",
                "foreignField": "_id",
                "as":           "vacancy"
            }
        },
        {"$unwind": "$vacancy"},
        {
            $match: {
                "vacancy.deadLine": {
                    $lte: global.time + 3600 * 24 * 5,
                    $gte: global.time
                }

            }
        },
        {
            $lookup: {
                "from":         "users",
                "localField":   "uid",
                "foreignField": "_id",
                "as":           "users"
            }
        },
        {"$unwind": "$users"},
        {
            $lookup: {
                "from":         "email_notifications",
                "localField":   "uid",
                "foreignField": "uid",
                "as":           "email_notifications"
            }
        },
        {
            $match: {
                $or: [
                    {
                        "email_notifications": {
                            $size: 0
                        }
                    },
                    {
                        "email_notifications": {
                            $exists: true,
                            $all:    [
                                {
                                    $elemMatch: {
                                        $or: [
                                            {
                                                "emails.options.5vacancyId": {$ne: "$vacancy._id"},
                                            },
                                            {
                                                'emails.options.5vacancyId': {
                                                    $exists: false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            $project: {
                "first_name":      "$users.first_name",
                "last_name":       "$users.last_name",
                "positionTitle":   "$vacancy.positionTitle",
                "email":           "$users.email",
                "uid":             "$users._id",
                "vacancyId":       "$vacancy._id",
                "currentLang":     "$users.currentLang",
                "prefLang":        "$users.prefLang",
                "eduPrefLang":     "$users.Educator.prefLang",
                "companyPrefLang": "$users.Company.prefLang",
                "studentPrefLang": "$users.Student.prefLang",
                "opt":             {"5vacancyId": "$vacancy._id"},
            }
        },
        {
            $match: {
                isSend: false
            }
        }
    ], function (err, data) {
        // email
        // currentLang
        global.log.info("<5>");
        global.log.info(data);

        if (err) {
            global.log.info("method m_5 error", err);
            callback(null, err);
        }
        else {
            global.mail.sendEmail(data, 5, callback);
        }
    });
}