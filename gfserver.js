async     = require('async');
global.argv   = require('minimist')(process.argv.slice(2));
global.config = global.argv;
global.env    = require('./lib/envirement');
var mail      = global.mail = require('./lib/email');
var api    = require('./lib/api');
global.log = require('./lib/logger');
_      = require('lodash');

var mongoClient = require("mongodb").MongoClient;
//var assert = require('assert');
ObjectId    = require('mongodb').ObjectID;
var time        = global.time = parseInt(new Date().getTime() / 1000);
var url              = "mongodb://project:project@localhost:27017/project";
global.db            = null;
global.email_options = {};
global.translate     = {};
global.global.notify = {};
global.api           = require('./lib/api');

let events = {
    5: require('./events/5'),
    6: require('./events/6'),
    7: require('./events/7'),
    8: require('./events/8'),
    9: require('./events/9'),
    13: require('./events/13'),
    14: require('./events/14'),
    16: require('./events/16'),
    17: require('./events/17'),
    18: require('./events/18'),
    20: require('./events/20'),
    31: require('./events/31'),
    33: require('./events/33'),
    34: require('./events/34'),
};

// var crypto = require('crypto');
//
// function hashPassword(password) {
//     const salt = crypto.randomBytes(16).toString('hex');
//     const hash = crypto.pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');
//     return [
//         salt,
//         hash
//     ].join('$');
// }
//
// // Checking the password hash
// function verifyHash(password, original) {
//     const originalHash = original.split('$')[1];
//     const salt         = original.split('$')[0];
//     const hash         = crypto.pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');
//
//     if (hash === originalHash) {
//         return true;
//     }
//     else {
//         return false;
//     }
// }
//
// function updatePasswords() {
//     mongoClient.connect(url, function (err, connect) {
//         global.db = connect
//
//         global.db.collection('users').find({
//             "password": {
//                 $exists: true
//             }
//         }, function (err, data) {
//             if (err) {
//                 global.log.info("err", err)
//             }
//             else {
//                 data.forEach(function (e) {
//                     let oldPass = e.password
//                     let newPass = hashPassword(e.password)
//                     console.log('-----');
//                     let verify = verifyHash(oldPass, newPass)
//                     console.log(e._id, verify);
//                     if (verify) {
//                         global.db.collection('users').updateOne({_id: ObjectId(e._id)}, {$set:{password: newPass}},
// function (err, d) { if (err) { console.error(err); } else { console.log('updated'); } }) } }) }  }) }) }  return
// updatePasswords();

function getTranslate(callback) {
    global.db.collection("options").findOne({"_id": "withoutTrasnslate"}, {
        "languages": 1,
        "_id":       0
    }, function (err, opt) {
        for (var i = 0; i < opt.languages.length; i++) {
            global.translate[opt.languages[i].id] = opt.languages[i].value;
        }
        callback(null, 'getTranslate true');
    });
}

function getOptions(callback) {
    global.db.collection("options").findOne({"_id": "translate"}, {
        "translate": 1,
        "_id":       0
    }, function (err, opt) {
        global.options = opt['translate'];
        callback(null, 'getOptions true');
    });
}

function getTemplate(callback) {
    global.db.collection("options").findOne({"_id": "email"}, function (err, opt) {
        global.email_options = opt;
        callback(null, 'getTemplate true');
    });
}

function startNotify() {

    mongoClient.connect(url, function (err, connect) {
        global.db   = connect;
        var functions  = [
            getOptions,
            getTranslate,
            getTemplate,
            events["6"],
            events["7"],
            events["8"],
            // events["34"],
        ];
        var current = new Date();
        global.log.info('time hour ' + current.getHours());
        global.log.info(current);
        if (current.getHours() == 8) {
            functions = functions.concat([
                events["5"],
                // events["9"],
                events["13"],
                events["14"],
                events["16"],
                events["17"],
                events["18"],
                // events["20"],
                events["31"],
                events["33"],
                // events["34"],
            ]);
        }
        async.series(functions, function (err, result) {
            global.log.info(result, result);
            db.close();
            process.exit(0);
        });
    });
}

var err = {'error': true};
global.log.error(err);


startNotify();

